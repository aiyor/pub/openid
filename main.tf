locals {
  public_key_pem = file("${path.module}/public_key.pem")

  config = yamldecode(file("${path.module}/config.yaml"))

  openid_key = {
    keys = [
      jsondecode(data.jwks_from_key.openid_key.jwks),
    ]
  }

  openid_config = {
    issuer   = local.config.openid_config.issuer
    jwks_uri = "${local.config.openid_config.issuer}/.well-known/jwks"
    response_types_supported = local.config.openid_config.response_types_supported
    subject_types_supported = local.config.openid_config.subject_types_supported
    id_token_signing_alg_values_supported = local.config.openid_config.id_token_signing_alg_values_supported
    claims_supported = local.config.openid_config.claims_supported
  }
}

data "jwks_from_key" "openid_key" {
  key = local.public_key_pem
  kid = local.config.jwks.kid
  use = local.config.jwks.use
  alg = local.config.jwks.alg
}

resource "local_file" "openid_keys" {
  content  = jsonencode(local.openid_key)
  filename = "${path.module}/public/.well-known/jwks"
}

resource "local_file" "openid_config" {
  content  = jsonencode(local.openid_config)
  filename = "${path.module}/public/.well-known/openid-configuration"
}
