variable "kid" {
  description = "Key ID for JWT."
  type = string
  default = "0xabc123"
}

variable "duration_in_minutes" {
  description = "Duration of JWT (in minutes) before expiry."
  type = number
  default = 60
}

variable "sub" {
    description = "Value of subject for JWT claim."
    type = string
    default = "sub:local:ci"
}

variable "aud" {
    description = "Value of audience for JWT claim."
    type = string
    default = "aud:local:ci"
}

variable "issuer" {
    description = "The URL of issuer of JWT."
    type = string
    default = "https://openid.definitely.work"
}
