terraform {
  backend "http" {
  }

  required_providers {
    jwks = {
      source  = "iwarapter/jwks"
      version = "0.1.0"
    }

    jwt = {
      source  = "camptocamp/jwt"
      version = "1.1.2"
    }

    time = {
      source  = "hashicorp/time"
      version = "0.11.1"
    }

    env = {
      source = "tcarreira/env"
      version = "0.2.0"
    }
  }
}

provider "jwks" {}
provider "jwt" {}
provider "time" {}
provider "env" {}